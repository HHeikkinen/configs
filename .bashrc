### --- START CUSTOM CONFIG --- ###

export DISPLAY=:0.0

alias pg='sudo -u postgres'
alias scode="sudo code --user-data-dir=$HOME"
alias https='http --default-scheme=https'
#### alias monitor='xrandr --output eDP-1 --primary --auto --output HDMI-2 --right-of eDP-1 --auto --output DP-1 --auto --right-of HDMI-2'

# Source grep
sgrep() { egrep -rn $1 src/$2; }

# cd to whereis
goto() { cd $(whereis $1 | awk '{print $2}'); }

# Get pulseaudio sink volume
getvolume() { pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,'; }

# Unlimited bash_history
# HISTSIZE=-1
HISTFILESIZE=-1

### --- END CUSTOM CONFIG --- ###
