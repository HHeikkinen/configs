# -*- coding: utf-8 -*-

import os

kbCommand = 'setxkbmap -query | grep layout | awk \'{print toupper($2)}\''

class Py3status:

    def keyboardLayout(self):
        layout = os.popen(kbCommand).read().strip()
        return {
            'full_text': layout,
            'cached_until': self.py3.time_in(5)
        }