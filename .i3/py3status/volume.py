# -*- coding: utf-8 -*-

import os

vol = "pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,';"

class Py3status:

    def volumeBar(self):
        volString = os.popen(vol).read().strip()
        return {
            'full_text': volString,
            'cached_until': self.py3.time_in(5)
        }